<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    //Accion o metodo del controlador
    //nombre cualquiera , recomendacion minuscula
    public function index(){
        //seleccionar las categorias existentes
        $categorias=Categoria::paginate(5);
        
        //enviar categorias a una vista y las mostramos
        return view("categorias.index")->with("categorias", $categorias);

        //recorrer cada categoria
        /* foreach($categorias as $c){
            echo " <pre>";
            var_dump($c->name);
            echo " </pre> <hr/>";
        }*/
    }
    //mostar formulario para crear categoria
    public function create(){
        //echo "Formulario de categoria";
        return view("categorias.new");
    }
    //llevar datos desde formulario y guardarlos en bd
    public function store(Request $r){
        //Validacion - 1.establecer reglas de validacion para cada campo
        $reglas =[
            "categoria"=>["required", "alpha"] 
        ];
        $mensajes=[
            "required"=>"Campo obligatorio",
            "alpha"=>"Solo se permiten letras"
        ];

        //2.crear el objeto validador
        $validador= Validator::make($r->all(), $reglas, $mensajes);//REVISAR
        //3.validar:metodo fails , si la validacion falla retorna true y si es correcta false 

        if($validador->fails()){
            //codigo cuando falla
            return redirect("categorias/create")->withErrors($validador);
        }else{
            //codigo si la validacion es correcta
        }

        //$_POST arreglo de php
        //almacena info de formulario
        var_dump($_POST);
        //crear nueva categoria
        $categoria=new Categoria();
        //asignar nombre
        //trae datos desde el campo del formulario "categoria"
        $categoria->name =$r->input("categoria");
        //guardar la nueva categoria
        $categoria->save();
        echo "Categoria Guardada";        
        //redireccion con datos de sesion
        return redirect('categorias/create')->with("mensaje", "Categoria Guardada");
    }

    public function edit($category_id){
        //Seleccionar la categoria a editar
        $categoria= Categoria::find($category_id);
        //mostrar la vista de actualizacion levando los datos de la categoria
        return view("categorias.edit")->with("categoria",$categoria);
    }
    public function update($category_id){
        //Seleccionar la categoria a editar
        $categoria= Categoria::find($category_id);
        //Editar sus atributos
        $categoria->name=$_POST["categoria"];
        //Guarda cambios
        $categoria->save();  
        //retornar formulario
        return redirect("categorias/edit/$category_id")->with("mensaje", "Categoria Editada");      
    }
}
