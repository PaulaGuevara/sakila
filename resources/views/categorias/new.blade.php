<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Añadir Categoria</title>
</head>
<body>
  <!-- Si la variable de sesion mensaje existe, la muestra -->
    @if(session("mensaje"))
    <p class="alert-sucess"> {{session("mensaje")}} </p>
    @endif
    
    <form method="POST" action=" {{ url('categorias/store')}} " class="form-horizontal">
        {{ csrf_field() }}
        <fieldset>

        <!-- Form Name -->
        <legend>Nueva Categoria</legend>
        
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="textinput">Nombre de Categoria</label>  
          <div class="col-md-4">
          <input id="textinput" name="categoria" type="text" placeholder="Categoria" class="form-control input-md">
          <strong class="text-danger">{{ $errors -> first("categoria") }}</strong>
          </div>
        </div>
        
        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="singlebutton"></label>
          <div class="col-md-4">
            <button id="singlebutton" name="singlebutton" class="btn btn-primary">Aceptar</button>
          </div>
        </div>
        
        </fieldset>
        </form>
</body>
</html>