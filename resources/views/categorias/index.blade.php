<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sakila</title>
</head>
<body>
    <h1>Lista de categorias</h1>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Nombre Categoria</th>
                <th>Actualizar</th>
            </tr>            
        </thead>
        
        <tboby>
            @foreach ($categorias as $c)
            <tr>
                <td>
                    {{ $c->name }}
                </td>
                <td>
                    <a href="{{url('categorias/edit/'.$c->category_id)}}">Actualizar</a>
                </td>
            </tr>            
            @endforeach 
        <tboby>
    </table> 
    {{ $categorias->links() }} 
</body>
</html> 